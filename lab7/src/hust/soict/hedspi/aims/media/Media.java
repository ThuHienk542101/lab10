package hust.soict.hedspi.aims.media;

public abstract class Media {
	protected String id;
	protected String title;
	protected String category;
	protected float cost;
	
	public String getId() {
		return id;
	}
	public String getTitle() {
		return title;
	}
	public String getCategory() {
		return category;
	}
	public float getCost() {
		return cost;
	}
	
	public Media() {};
	
	public Media(String id, String title, String category, float cost) {
		this.id = id;
		this.title=title;
		this.category=category;
		this.cost=cost;
	}
	
	public boolean search(String title) {
		String[] parts= title.split(" ");
		String[] parts_title= this.title.split(" ");
	    for(int i=0; i<parts.length; i++) {
	    	boolean test=false;
	    	for(int j=0; j<parts_title.length; j++) {
	    		 if(parts[i].equals(parts_title[j])) {
	    			 test = true;
	    			 break;
	    		 }
	    	}
	    	if(!test) {
	    		return false;
	    	}
	    }
		return true;
	}
}
