package hust.soict.hedspi.aims.media;
import java.util.ArrayList;
import java.util.List;


public class Book extends Media{
	private  List<String> authors = new ArrayList<String>();
	public Book() {}
	
	public Book(String id, String title, String category, float cost) {
		super(id, title, category, cost);
	}
	
	public Book(String id, String title, String category, float cost, List<String> authors) {
		super(id, title, category, cost);
		if(authors.size()!=0) {
			this.authors=authors;
		}else {
			System.out.println("authors empty!");
		}
	}
	
	public void addAuthor(String authorName) {
		if(!(authors.contains(authorName))) {
			authors.add(authorName);
		}else {
			System.out.println("Author has been existed");
		}
	}
	
	public void removeAuthor(String authorName) {
		if((authors.contains(authorName))) {
			authors.remove(authorName);
		}else {
			System.out.println("Author don't existed");
		}
	}
	
	public List<String> getAuthors() {
		return authors;
	}
	
	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
}