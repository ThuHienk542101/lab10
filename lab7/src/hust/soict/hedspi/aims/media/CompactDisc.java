package hust.soict.hedspi.aims.media;
import java.util.ArrayList;
import java.util.List;

public class CompactDisc extends Disc implements Playable{
	private String artist;
	private List<Track> tracks = new ArrayList<Track>();
	
	public void setArtist(String artist) {
		this.artist = artist;
	}

	public String getArtist() {
		return artist;
	}
	
	public void addTrack(Track track) {
		for(int i=0; i<tracks.size(); i++) {
			if(tracks.get(i).getTitle().equals(track.getTitle())){
				System.out.println("Track has been existed");
				return;
			}
		}
		tracks.add(track);
	}
	
	public void removeTrack(Track track) {
		if((tracks.contains(track))) {
			tracks.remove(track);
		}else {
			System.out.println("Track don't existed");
		}
	}
	
	public int getLength() {
		int sum=0;
		for(int i=0; i<tracks.size(); i++) {
			sum+=tracks.get(i).getLength();
		}
		super.length=sum;
		return sum;
	}
	
	public CompactDisc() {}
	
	public CompactDisc(String id, String title, String category, float cost) {
		super(id, title, category, cost);
	}	
	
	public CompactDisc(String id, String title, String category, float cost, String artist) {
		super(id, title, category, cost);
		this.artist = artist;
	}
	
	public void play() {
		System.out.println("Playing CompactDisc: " + this.getTitle());
		System.out.println("CompactDisc length: " + this.getLength());
		for(int i=0; i<tracks.size(); i++) {
			tracks.get(i).play();
		}
	}
}