package hust.soict.hedspi.aims.media;

public class DigitalVideoDisc extends Disc implements Playable{
	
	public boolean setLength(int length) {
		if(length > 0) { 
			this.length = length;
			return true;
		}else 
			return false;
	}
	
	public void setDirector(String director) {
		this.director = director;
	}
	
	public DigitalVideoDisc() {}
	
	public DigitalVideoDisc(String id, String title, String category, float cost) {
		super(id, title, category, cost);
	}	
	
	public DigitalVideoDisc(String id, String title, String category, float cost, int length, String director) {
		super(id, title, category, cost, length, director);
	}
	
	public void play() {
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}
}
