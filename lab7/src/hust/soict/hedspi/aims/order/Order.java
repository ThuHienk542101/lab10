package hust.soict.hedspi.aims.order;
import hust.soict.hedspi.aims.utils.*;
import hust.soict.hedspi.aims.media.*;

import java.lang.Math;
import java.util.ArrayList;
import java.util.List;

public class Order {
	
	public static final int MAX_NUMBERS_ORDERED = 3;
	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	//	private int qtyOrdered;
	private MyDate dateOrdered;
	public static int nbOrders=0;
	public static final int MAX_LIMITTED_ORDERS=3;
	public int lucky=-1;
	
	public MyDate getDateOrdered() {
		return dateOrdered;
	}
	
	public void setDateOrdered(int day, int month, int year) {
		dateOrdered.setDay(day);
		dateOrdered.setMonth(month);
		dateOrdered.setYear(year);
	}
	
	public void setDateOrdered(String date) {
		dateOrdered.trans(date);
	}
	
	public void addMedia(Media media) {
		for(int i=0; i<itemsOrdered.size(); i++) {
			if(itemsOrdered.get(i).getId().equals(media.getId())) {
				System.out.println("id nay da ton tai!");
				return;
			}
		}
		itemsOrdered.add(media);
	}
	
	public void removeMedia(Media media) {
		itemsOrdered.remove(media);
	}
	
	public void removeMedia(String id) {
		for(int i=0; i<itemsOrdered.size(); i++) {
			if(itemsOrdered.get(i).getId().equals(id)) {
				itemsOrdered.remove(itemsOrdered.get(i));
				System.out.println("xoa thanh cong!");
				return;
			}
		}
		System.out.println("ko tim thay media co id = "+id);
	}
	
	
	public float totalCost() {
		float total = 0;
		for(int i=0; i<itemsOrdered.size(); i++) {
			total += this.itemsOrdered.get(i).getCost();
		}
		return total;
	}
	
	public void printOrdered() {
		System.out.println("********************Order********************");
		System.out.println("Date: "+dateOrdered.getDay()+"-"+dateOrdered.getMonth()+"-"+dateOrdered.getYear());
		System.out.println("Ordered Items: ");
		for(int i=0; i<itemsOrdered.size(); i++) {
			System.out.print(itemsOrdered.get(i).getId());
			System.out.print(" - " + itemsOrdered.get(i).getTitle());
			System.out.print(" - " + itemsOrdered.get(i).getCategory());
			System.out.print(": " + itemsOrdered.get(i).getCost()+"$\n");
		}
		System.out.println("Total cost: "+totalCost()+"$");
		System.out.println("*********************************************");
	}
	
	public Order() {
		if(this.nbOrders >= MAX_LIMITTED_ORDERS) {
			System.out.println("So luong don hang vuot qua cho phep!");
			System.exit(0);
		}
		if(this.nbOrders >0) {
			System.out.println("Tao don hang moi thanh cong!");
		}
//		for(int i=0; i<itemsOrdered.size(); i++) {
//			itemsOrdered.remove(media);
//		}
		dateOrdered = new MyDate();
		nbOrders++;
	}
	
	public Media getALuckyItem() {
		lucky = (int)(Math.random() * itemsOrdered.size();
		Media tmp = itemsOrdered.get(lucky);
		removeMedia(itemsOrdered.get(lucky));
		return tmp;
	}
}