package hust.soict.hedspi.aims.utils;
public class DateUtils {
	 public static int compareMyDate(MyDate dateA, MyDate dateB) {
	       if(dateA.getYear()<dateB.getYear()) {
	    	   return 1; //A>B
	       }else if(dateA.getYear()>dateB.getYear()) {
	    	   return -1;	//B>A
	       }else {
	    	   if(dateA.getMonth()<dateB.getMonth()) {
	    		   return 1;
	    	   }else if(dateA.getMonth()>dateB.getMonth()){
	    		   return -1;
	    	   }else {
	    		   if(dateA.getDay()<dateB.getDay()) {
	    			   return 1;
	    		   }else if(dateA.getDay()>dateB.getDay()) {
	    			   return -1;
	    		   }else {
	    			   return 0; 	//A=B
	    		   }
	    	   }
	       }
	 }
	    
	 public static void sortMyDate(MyDate[] dateList) {
	        for(int i=0; i<dateList.length; i++) {
	        	for(int j=0; j<dateList.length-1; j++) {
	        		if(compareMyDate(dateList[j], dateList[j+1]) == -1) {
	        			MyDate tmp = new MyDate(dateList[j].getDay(), dateList[j].getMonth(), dateList[j].getYear());
	        			dateList[j].setDay(dateList[j+1].getDay());
	        			dateList[j].setMonth(dateList[j+1].getMonth());
	        			dateList[j].setYear(dateList[j+1].getYear());
	        			dateList[j+1].setDay(tmp.getDay());
	        			dateList[j+1].setMonth(tmp.getMonth());
	        			dateList[j+1].setYear(tmp.getYear());
	        		}
	        	}
	        }
	 }
	    
	 public static void print(MyDate[] dateList) {
	      for(int i=0; i<dateList.length; i++) {
	    	  System.out.println(dateList[i].getDay()+"-"+dateList[i].getMonth()+"-"+dateList[i].getYear());
	      }
	 }
}