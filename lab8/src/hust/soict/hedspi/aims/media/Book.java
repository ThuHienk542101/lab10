package hust.soict.hedspi.aims.media;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Book extends Media implements java.lang.Comparable<Object>{
	private  List<String> authors = new ArrayList<String>();
	private String content;
	public List<String>contentTokens =  new ArrayList<String>();
	public Map<String, Integer>wordFrequency = new TreeMap<String, Integer>();
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
		processContent();
	}
	
	public Book() {}
	
	public Book(String id, String title, String category, float cost) {
		super(id, title, category, cost);
	}
	
	public Book(String id, String title, String category, float cost, List<String> authors) {
		super(id, title, category, cost);
		if(authors.size()!=0) {
			this.authors=authors;
		}else {
			System.out.println("authors empty!");
		}
	}
	
	public void addAuthor(String authorName) {
		if(!(authors.contains(authorName))) {
			authors.add(authorName);
		}else {
			System.out.println("Author has been existed");
		}
	}
	
	public void removeAuthor(String authorName) {
		if((authors.contains(authorName))) {
			authors.remove(authorName);
		}else {
			System.out.println("Author don't existed");
		}
	}
	
	public List<String> getAuthors() {
		return authors;
	}
	
	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	
	@Override
	public int compareTo(Object obj) {
		if(obj instanceof Book) {
			Book book = (Book) obj;
			return this.getTitle().compareTo(book.getTitle());
		}else {
			return super.compareTo(obj);
		}
	}
	
	private void processContent() {
		contentTokens.addAll(Arrays.asList(content.split("\\s+")));
		Collections.sort(contentTokens);
		Iterator<String> iterator = contentTokens.iterator();
		while(iterator.hasNext()) {
			String string = iterator.next();
			if (wordFrequency.containsKey(string) == false) {
				wordFrequency.put(string, 1);
			}else {
				int a = wordFrequency.get(string);
				a++;
				wordFrequency.put(string, a);
			}
		}
	}
	
	@Override
	public String toString() {
		String string = "";
		string += "Id: " + super.id + "\n";
		string += "Title: "	+ super.title + "\n";
		string += "Category: " + super.category + "\n";
		string += "Cost: " + super.cost + "\n";
		string += "Number of tokens: " + contentTokens.size() + "\n";
		for(Map.Entry<String, Integer> entry: wordFrequency.entrySet()) {
			string += entry.getKey() + " - "+ entry.getValue() + "\n";
		}
		return string;
	}
}