package hust.soict.hedspi.test.book;

import hust.soict.hedspi.aims.media.*;

public class BookTest {

	public static void main(String[] args) {
		Book b1 = new Book("1", "a123", "laksd", 712);
		b1.setContent("hom nay toi di hoc oop. hoc oop rat kho");
		System.out.println(b1.getContent());
		System.out.println(b1.contentTokens);
		System.out.println(b1.wordFrequency);
		System.out.println("========================================");
		System.out.println(b1.toString());
	}

}
