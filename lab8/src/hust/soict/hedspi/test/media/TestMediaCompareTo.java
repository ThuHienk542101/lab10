package hust.soict.hedspi.test.media;

import java.util.*;

//import hust.soict.hedspi.aims.exceptions.PlayerException;
import hust.soict.hedspi.aims.media.*;

public class TestMediaCompareTo {

	public static void main(String[] args) {
		java.util.Collection<Media> collection = new java.util.ArrayList<>();
	
		// Data test
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("7","Aladdin", "acd", 213);
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("6","Star War", "abc", 321);
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("5","The Lion King", "abc", 123);
		CompactDisc cd1 = new CompactDisc("4","Doraemon", "hoat hinh", 12);
		CompactDisc cd2 = new CompactDisc("3","Connan", "trinh tham", 15);
		Book b1 = new Book("1","Shiba", "dog", 821);
		Book b2 = new Book("2","Bug", "dog", 21);
//		collection.add(b1);
//		collection.add(b2);
		collection.add(dvd2);
		collection.add(dvd3);
		collection.add(dvd1);
//		collection.add(cd1);
//		collection.add(dvd2);
//		collection.add(cd2);
		display(collection);
		
	}
	
	private static void display(java.util.Collection<Media> collection) {
		Iterator<Media> iterator = collection.iterator();
		iterator = collection.iterator();
		System.out.println("----------------------------------");
		System.out.printf("%-3s | %-6s | %-5s | %-15s | %-10s : %-12s| %-5s | %-5s%n", "STT", "Type", "ID", "Title", "Category", "Total", "Number", "Length");
		int i = 1;
		while(iterator.hasNext()) {
			Media media = iterator.next();
			if(media instanceof Book) {
				System.out.printf("%-3s | %-6s | %-5s | %-15s | %-10s : %-10s$ |%n", i, "Book", media.getId(), media.getTitle(), media.getCategory(), media.getCost());
			}else if(media instanceof DigitalVideoDisc) {
				System.out.printf("%-3s | %-6s | %-5s | %-15s | %-10s : %-10s$ |%n", i, "DVD", media.getId(), media.getTitle(), media.getCategory(), media.getCost());
			}else if(media instanceof CompactDisc) {
				System.out.printf("%-3s | %-6s | %-5s | %-15s | %-10s : %-10s$ | %-5s | %-5s%n", i, "CD", media.getId(), media.getTitle(), media.getCategory(), media.getCost(), ((CompactDisc)media).getTracks().size(), ((CompactDisc) media).getLength());
			}
			i++;
		}
		
//		java.util.Collections.sort((Collection<Media>)collection);
		Collections.sort((ArrayList<Media>)collection);
		iterator = collection.iterator();
		System.out.println("----------------------------------");
		System.out.printf("%-3s | %-6s | %-5s | %-15s | %-10s : %-12s| %-5s | %-5s%n", "STT", "Type", "ID", "Title", "Category", "Total", "Number", "Length");
		i = 1;
		while(iterator.hasNext()) {
			Media media = iterator.next();
			if(media instanceof Book) {
				System.out.printf("%-3s | %-6s | %-5s | %-15s | %-10s : %-10s$ |%n", i, "Book", media.getId(), media.getTitle(), media.getCategory(), media.getCost());
			}else if(media instanceof DigitalVideoDisc) {
				System.out.printf("%-3s | %-6s | %-5s | %-15s | %-10s : %-10s$ |%n", i, "DVD", media.getId(), media.getTitle(), media.getCategory(), media.getCost());
			}else if(media instanceof CompactDisc) {
				System.out.printf("%-3s | %-6s | %-5s | %-15s | %-10s : %-10s$ | %-5s | %-5s%n", i, "CD", media.getId(), media.getTitle(), media.getCategory(), media.getCost(), ((CompactDisc)media).getTracks().size(), ((CompactDisc) media).getLength());
			}
			i++;
		}
		System.out.println("----------------------------------");
	}
}

