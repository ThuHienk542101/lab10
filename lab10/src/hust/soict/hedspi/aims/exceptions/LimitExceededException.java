package hust.soict.hedspi.aims.exceptions;

public class LimitExceededException extends Exception{
	public LimitExceededException(String msg) {
		super(msg);
	}
	
	public LimitExceededException(String msg, Throwable cause) {
		super(msg, cause);
	}
}