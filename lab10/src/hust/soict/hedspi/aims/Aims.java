package hust.soict.hedspi.aims;
import hust.soict.hedspi.aims.order.*;
import java.util.ArrayList;
import java.util.List;

import hust.soict.hedspi.aims.media.*;
import hust.soict.hedspi.aims.utils.*;
import hust.soict.hedspi.aims.thread.*;
import hust.soict.hedspi.aims.exceptions.*;
import java.util.Scanner;
public class Aims {
	public static void main(String[] args) {
		MemoryDaemon m = new MemoryDaemon();
		Thread t = new Thread();
		t.setDaemon(true);;
		Order anOrder = null;
		try {
			anOrder = new Order();
		} catch (LimitExceededException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int check=0;
		do {
			int choose;
			Scanner sc = new Scanner(System.in);
			System.out.println("--------------------------------");
			System.out.println("1. Create neworder");
			System.out.println("2. Add item to the order");
			System.out.println("3. Delete item by id");
			System.out.println("4. Display the items list of order");
			System.out.println("0. Exit");
			System.out.println("--------------------------------");
			System.out.print("Your choice is: ");
			choose = sc.nextInt();
			switch(choose) {
				case 0:
					return;
				case 1:
					if(check ==0 ) {
						System.out.println("Tao don hang moi thanh cong!");
					}else {
						try {
							anOrder = new Order();
						} catch (LimitExceededException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							System.exit(0);
						}
					}
					check=1;
					break;
				case 2:
					if(check==0) {
						System.out.println("Hay tao don hang moi");
					}else {
						System.out.print("Add book(1), dvd(2) or cd(3): ");
						int choose2=0;
						choose2 = sc.nextInt();
						if(choose2==1) {
							sc.nextLine();
							System.out.print("Add book - id: ");
							String id = sc.nextLine();
							System.out.print("title: ");
							String title = sc.nextLine();
							System.out.print("category: ");
							String category = sc.nextLine();
							System.out.print("cost: ");
							float cost = sc.nextFloat();
							Book book = new Book(id, title, category, cost);
							System.out.print("So tac gia: ");
							int n = sc.nextInt();
							sc.nextLine();
							List<String> authors = new ArrayList<String>();
							for(int i=0; i<n; i++) {
								String name = sc.nextLine();
								authors.add(name);
							}
							book.setAuthors(authors);
							anOrder.addMedia(book);
//							sc.nextLine();
						}else if(choose2==2) {
							sc.nextLine();
							System.out.print("add dvd - id: ");
							String id = sc.nextLine();
							System.out.print("title: ");
							String title = sc.nextLine();
							System.out.print("category: ");
							String category = sc.nextLine();
							System.out.print("cost: ");
							float cost = sc.nextFloat();
							DigitalVideoDisc dvd = new DigitalVideoDisc(id, title, category, cost);
							System.out.print("length: ");
							int length = sc.nextInt();
							dvd.setLength(length);
							System.out.print("director: ");
							String director = sc.nextLine();
							sc.nextLine();
							dvd.setDirector(director);
							anOrder.addMedia(dvd);
							System.out.print("Ban co muon phat DVD(y/n): ");
							String choose3 = sc.nextLine();
							if(choose3.equals("y")||choose3.equals("Y")) {
								try {
									dvd.play();
								} catch (PlayerException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
//							sc.nextLine();
						}else if(choose2==3) {
							sc.nextLine();
							System.out.print("add cd - id: ");
							String id = sc.nextLine();
							System.out.print("title: ");
							String title = sc.nextLine();
							System.out.print("category: ");
							String category = sc.nextLine();
							System.out.print("cost: ");
							float cost = sc.nextFloat();
							CompactDisc cd = new CompactDisc(id, title, category, cost);
							System.out.print("artist: ");
							String artist = sc.nextLine();
							sc.nextLine();
							System.out.print("number of tracks: ");
							cd.setArtist(artist);
							int n = sc.nextInt();
							for(int i=0; i<n; i++) {
								sc.nextLine();
								System.out.print("track " +(i+1)+ " - title: ");
								title = sc.nextLine();
								System.out.print("length: ");
								int length = sc.nextInt();
								Track track = new Track(title, length);
								cd.addTrack(track);
							}
							anOrder.addMedia(cd);
							sc.nextLine();
							System.out.print("Ban co muon phat CD(y/n): ");
							String choose3 = sc.nextLine();
//							System.out.println(choose3);
							if(choose3.equals("y")||choose3.equals("Y")) {
//								System.out.println("ok");
								try {
									cd.play();
								} catch (PlayerException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
//							sc.nextLine();
						}
						else {
							System.out.println("Chon 1 hoac 2 hoac 3!");
						}
					}
					break;
				case 3:
					if(check==0) {
						System.out.println("Hay tao don hang moi");
					}else {
						sc.nextLine();
						System.out.print("remove item has id = ");
						String idRemove = sc.nextLine();
						anOrder.removeMedia(idRemove);
					}
					break;
				case 4:
					if(check==0) {
						System.out.println("Hay tao don hang moi");
					}else {
						try {
							Media lucky = anOrder.getALuckyItem();
							if(lucky!=null) {
								System.out.print("ban da du dieu kien nhan qua: ");
								System.out.println("title: "+lucky.getTitle()+" - Cost: "+lucky.getCost());
							}
						} catch (LuckyItemException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						anOrder.printOrdered();
					}
					break;
				default:
					System.out.println("Please choose a number: 0-1-2-3-4");
					break;
			}
		}while(true);
	}
	
}