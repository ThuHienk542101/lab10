package hust.soict.hedspi.aims.media;

import hust.soict.hedspi.aims.exceptions.*;

public class DigitalVideoDisc extends Disc implements Playable, java.lang.Comparable<Object>{
	
	public boolean setLength(int length) {
		if(length > 0) { 
			this.length = length;
			return true;
		}else 
			return false;
	}
	
	public void setDirector(String director) {
		this.director = director;
	}
	
	public DigitalVideoDisc() {}
	
	public DigitalVideoDisc(String id, String title, String category, float cost) {
		super(id, title, category, cost);
	}	
	
	public DigitalVideoDisc(String id, String title, String category, float cost, int length, String director) {
		super(id, title, category, cost, length, director);
	}
	@Override
	public void play() throws PlayerException{
		if(this.getLength() > 0) {
			System.out.println("Playing DVD: " + this.getTitle());
			System.out.println("DVD length: " + this.getLength());
		}else {
			throw new PlayerException("ERROR: DVD length is non-positive");
		}
	}
	@Override
	public int compareTo(Object obj) {
		if(obj instanceof DigitalVideoDisc) {
			if(this.getCost() < ((DigitalVideoDisc) obj).getCost())
				return -1;
			else if(this.getCost() == ((DigitalVideoDisc) obj).getCost())
				return 0;
			else return 1;
		}else {
			return super.compareTo(obj);
		}
	}
}
