package hust.soict.hedspi.aims.order;
import hust.soict.hedspi.aims.utils.*;
import hust.soict.hedspi.aims.exceptions.*;
import hust.soict.hedspi.aims.media.*;

import java.lang.Math;
import java.util.*;

public class Order {
	
	public static final int MAX_NUMBERS_ORDERED = 3;
//	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	private List<Media> itemsOrdered = new ArrayList<Media>();
	//	private int qtyOrdered;
	private MyDate dateOrdered;
	public static int nbOrders=0;
	public static final int MAX_LIMITTED_ORDERS=3;
	public int lucky=-1;
	
	private static final int numberOfOrder = 4;
	private static final float thresholdsTotal = 300;
	private static final float thresholdsSale = 0.2f;
	
	public MyDate getDateOrdered() {
		return dateOrdered;
	}
	
	public void setDateOrdered(int day, int month, int year) {
		dateOrdered.setDay(day);
		dateOrdered.setMonth(month);
		dateOrdered.setYear(year);
	}
	
	public void setDateOrdered(String date) {
		dateOrdered.trans(date);
	}
	
	public void addMedia(Media media) {
		for(int i=0; i<itemsOrdered.size(); i++) {
			if(itemsOrdered.get(i).equals(media)) {
				System.out.println("id nay da ton tai!");
				return;
			}
		}
		itemsOrdered.add(media);
	}
	
	public void removeMedia(Media media) {
		itemsOrdered.remove(media);
	}
	
	public void removeMedia(String id) {
		for(int i=0; i<itemsOrdered.size(); i++) {
			if(itemsOrdered.get(i).getId().equals(id)) {
				itemsOrdered.remove(itemsOrdered.get(i));
				System.out.println("xoa thanh cong!");
				return;
			}
		}
		System.out.println("ko tim thay media co id = "+id);
	}
	
	
	public float totalCost() {
		float total = 0;
		for(int i=0; i<itemsOrdered.size(); i++) {
			total += this.itemsOrdered.get(i).getCost();
		}
		return total;
	}
	
	public void printOrdered() {
		Collections.sort((ArrayList<Media>)itemsOrdered);
		System.out.println("********************Order********************");
		System.out.println("Date: "+dateOrdered.getDay()+"-"+dateOrdered.getMonth()+"-"+dateOrdered.getYear());
		System.out.println("Ordered Items: ");
		for(int i=0; i<itemsOrdered.size(); i++) {
			System.out.print(itemsOrdered.get(i).getId());
			System.out.print(" - " + itemsOrdered.get(i).getTitle());
			System.out.print(" - " + itemsOrdered.get(i).getCategory());
			System.out.print(": " + itemsOrdered.get(i).getCost()+"$\n");
		}
		System.out.println("Total cost: "+totalCost()+"$");
		System.out.println("*********************************************");
	}
	
	public Order() throws LimitExceededException{
		if(this.nbOrders < MAX_LIMITTED_ORDERS) {
			if(this.nbOrders >0) {
				System.out.println("Tao don hang moi thanh cong!");
			}
			dateOrdered = new MyDate();
			nbOrders++;
		}else {
			throw new LimitExceededException("So luong don hang vuot qua cho phep!");
		}
	}
	
	public Media getALuckyItem() throws LuckyItemException{
		if(itemsOrdered.size() >= numberOfOrder && this.totalCost() >= thresholdsTotal) {
			if(checkOrder(this)) {
				int item = 0;
				do {
					double rand = Math.random();
					rand *= itemsOrdered.size();
					item = (int)rand;
				} while (itemsOrdered.get(item).getCost() <= this.totalCost()*thresholdsSale);
				return itemsOrdered.get(item);
			}else {
				throw new LuckyItemException("Tất cả hàng đều lớn hơn ngưỡng quà tặng (" + totalCost()*thresholdsSale + ")");
			}
		}else {
			throw new LuckyItemException("Không đủ điều kiện nhận sản phẩm may mắn\nSố sản phẩm >= " + numberOfOrder + "\nTổng đơn hàng >= " + thresholdsTotal);
		}
	}
	
	private boolean checkOrder(Order order) {
		for(Media media: order.itemsOrdered) {
			if(media.getCost() <= order.totalCost()*thresholdsSale)
				return true;
		}
		return false;
	}
}