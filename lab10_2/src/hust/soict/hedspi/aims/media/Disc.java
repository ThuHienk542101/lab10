package hust.soict.hedspi.aims.media;

public class Disc extends Media{
	protected String director;
	protected int length;
	
	public String getDirector() {
		return director;
	}

	public int getLength() {
		return length;
	}
	
	public Disc() {}
	
	public Disc(String id, String title, String category, float cost) {
		super(id, title, category, cost);
	}
	
	public Disc(String id, String title, String category, float cost, int length, String director) {
		super(id, title, category, cost);
		this.length = length;
		this.director = director;
	}
	
}
