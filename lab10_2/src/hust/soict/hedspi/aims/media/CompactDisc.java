package hust.soict.hedspi.aims.media;
import java.util.ArrayList;
import java.util.List;

import hust.soict.hedspi.aims.exceptions.PlayerException;

public class CompactDisc extends Disc implements Playable, java.lang.Comparable<Object>{
	private String artist;
	private List<Track> tracks = new ArrayList<Track>();
	public String message;
	
	
	
	public void setArtist(String artist) {
		this.artist = artist;
	}

	public String getArtist() {
		return artist;
	}
	
	public void addTrack(Track track) {
		for(int i=0; i<tracks.size(); i++) {
			if(tracks.get(i).equals(track)){
				System.out.println("Track has been existed");
				return;
			}
		}
		tracks.add(track);
	}
	
	public void removeTrack(Track track) {
		if((tracks.contains(track))) {
			tracks.remove(track);
		}else {
			System.out.println("Track don't existed");
		}
	}
	
	public List<Track> getTracks() {
		return tracks;
	}

	public int getLength() {
		int sum=0;
		for(int i=0; i<tracks.size(); i++) {
			sum+=tracks.get(i).getLength();
		}
		super.length=sum;
		return sum;
	}
	
	public CompactDisc() {}
	
	public CompactDisc(String id, String title, String category, float cost) {
		super(id, title, category, cost);
	}	
	
	public CompactDisc(String id, String title, String category, float cost, String artist) {
		super(id, title, category, cost);
		this.artist = artist;
	}
	
	public void play() throws PlayerException{
		System.out.println("Playing CD: " + this.getTitle());
		System.out.println("CD length: " + this.getLength());
		this.message = "Playing CD: " + this.getTitle() + "\nCD length: " + this.getLength();
		if(this.getLength() > 0) {
			for(Track track: tracks) {
				track.play();
				message += track.getMessage();
			}
		}else {
			throw new PlayerException("ERROR: CD length is non-positive");
		}
	}
	
	@Override
	public int compareTo(Object obj) {
		if(obj instanceof CompactDisc) {
			CompactDisc cd = (CompactDisc)obj;
			if(tracks.size() == cd.tracks.size()) {
				if(this.length < cd.getLength())
					return -1;
				else if(this.length == cd.getLength())
					return 0;
				else return 1;
			}
			else if(tracks.size() < cd.tracks.size())
				return -1;
			else return 1;
		}else {
			return super.compareTo(obj);
		}
	}
}